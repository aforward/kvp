defmodule Kvp.Worker do
  use GenServer
  alias Kvp.{Worker,Pair}

  ### Public API

  def start_link() do
    {:ok, _pid} = GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def set(k, v) do
    :ok = GenServer.cast(Worker, {:set, k, v})
  end

  def get(k) do
    GenServer.call(Worker, {:get, k})
  end

  def clear do
    :ok = GenServer.cast(Worker, :clear)
  end

  ### Server Callbacks

  def init(_) do
    {:ok, zero_state}
  end

  def handle_cast({:set, k, v}, state) do
    Pair.set(k, v)
    {:noreply, state |> Map.put(k, v)}
  end

  def handle_cast(:clear, _state) do
    Pair.clear
    {:noreply, zero_state}
  end

  def handle_call({:get, k}, _from, state) do
    {:reply, state |> Map.get(k), state}
  end

  defp zero_state, do: Pair.all
end
