defmodule Kvp.Pair do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query
  alias Kvp.{Pair, Repo}

  @primary_key {:name, :string, []}
  schema "pairs" do
    field :data, :map
    timestamps
  end

  @required_fields ~w(name data)
  @optional_fields ~w()

  def changeset(schema, params) do
    schema
    |> cast(params, @required_fields, @optional_fields)
    |> unique_constraint(:name)
  end

  @doc ~S"""
  Retrieve the data for a named key.

  ## Examples

      iex> Kvp.Pair.set("apple", "delicious")
      ...> Kvp.Pair.get("apple")
      "delicious"

      iex> Kvp.get("orange")
      nil
  """
  def get(name) do
    Pair |> Repo.get_by(name: name) |> clean
  end

  @doc ~S"""
  Set data for a named key.

  ## Examples

      iex> Kvp.Pair.set("apple", "delicious")
      ...> Kvp.Pair.get("apple")
      "delicious"

      iex> Kvp.set("apple", "delicious")
      ...> Kvp.set("apple", %{kind: "delicious"})
      ...> Kvp.get("apple")
      %{kind: "delicious"}
  """
  def set(name, data) do
    now = Ecto.DateTime.utc
    # TODO: Waiting on upsert in ecto
    sql = "INSERT INTO pairs (name, data, inserted_at, updated_at)
           VALUES ($1, $2, $3, $3)
           ON CONFLICT (name) DO UPDATE
           SET (data, updated_at) = ($2, $3)
           WHERE EXCLUDED.name = $1;"
    params = [name, data, now]
    Ecto.Adapters.SQL.query(Repo, sql, params)
  end

  @doc ~S"""
  Clear all keys from the store

  ## Examples

      iex> Kvp.Pair.set("apple", "delicious")
      ...> Kvp.Pair.clear
      ...> Kvp.Pair.get("apple")
      nil
  """
  def clear do
    Ecto.Adapters.SQL.query(Repo, "TRUNCATE pairs", [])
  end

  @doc ~S"""
  Return a map of all key values in the store.

  ## Examples

      iex> Kvp.Pair.clear
      ...> Kvp.Pair.set("apple", "delicious")
      ...> Kvp.Pair.set("orange", "tangerine")
      ...> Kvp.Pair.all
      %{"apple" => "delicious", "orange" => "tangerine"}
  """
  def all do
    Repo.all(from o in Pair)
    |> Enum.map(fn(pair) -> {pair.name, pair.data} end)
    |> Map.new
  end

  defp clean(nil), do: nil
  defp clean(%Pair{data: data}), do: data

end
