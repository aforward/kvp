defmodule Kvp do
  use Application
  def start(_type, _args) do
    import Supervisor.Spec, warn: false
    children = [
      supervisor(Kvp.Repo, []),
      worker(Kvp.Worker, []),
    ]
    opts = [strategy: :one_for_one, name: Kvp.Supervisor]
    Supervisor.start_link(children, opts)
  end

  @doc ~S"""
  Set a value to a specific key

  ## Examples

      iex> Kvp.set("apple", "delicious")
      ...> Kvp.get("apple")
      "delicious"

      iex> Kvp.set("apple", "delicious")
      ...> Kvp.set("apple", %{kind: "delicious"})
      ...> Kvp.get("apple")
      %{kind: "delicious"}
  """
  defdelegate set(k, v), to: Kvp.Worker

  @doc ~S"""
  Retrieve a value from the store, or nil if the key doesn't exist

  ## Examples

      iex> Kvp.clear
      ...> Kvp.get("apple")
      nil

      iex> Kvp.set("apple", "delicious")
      ...> Kvp.get("apple")
      "delicious"
  """
  defdelegate get(k), to: Kvp.Worker

  @doc ~S"""
  Clear all keys from the store

  ## Examples

      iex> Kvp.set("apple", "delicious")
      ...> Kvp.clear
      ...> Kvp.get("apple")
      nil
  """
  defdelegate clear, to: Kvp.Worker
end
