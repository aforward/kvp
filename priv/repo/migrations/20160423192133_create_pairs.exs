defmodule Kvp.Repo.Migrations.CreatePairs do
  use Ecto.Migration

  def change do
    create table(:pairs, primary_key: false) do
      add :name, :string, primary_key: true
      add :data, :map
      timestamps
    end
  end
end
